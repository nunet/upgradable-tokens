let NuNetUpgradeableToken = artifacts.require("./NuNetUpgradeableToken.sol");
let NuNetUpgradeableTokenV2 = artifacts.require("./NuNetUpgradeableTokenV2.sol");
let web3 = require("web3");

const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

module.exports = async function (deployer) {

	  const existing = await NuNetUpgradeableToken.deployed();
	  console.log("existing adress", existing.address);

      const instance = await upgradeProxy(existing.address, NuNetUpgradeableTokenV2,{deployer});
      
      console.log("Upgraded", instance.address);
};
