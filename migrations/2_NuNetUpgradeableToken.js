let NuNetUpgradeableToken = artifacts.require("./NuNetUpgradeableToken.sol");
let web3 = require("web3");

const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const name = "NuNet Utility Token";
const symbol = "NTX";
const supply = "1000000";

module.exports = async function (deployer) {
	 
      const instance = await deployProxy(NuNetUpgradeableToken,[name, symbol,web3.utils.toWei(supply)] , { deployer, initializer: 'initialize'});
      console.log('Deployed', instance.address);
      
};
