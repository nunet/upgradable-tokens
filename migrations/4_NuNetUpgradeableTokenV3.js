let NuNetUpgradeableTokenV2 = artifacts.require("./NuNetUpgradeableTokenV2.sol");
let NuNetUpgradeableTokenV3 = artifacts.require("./NuNetUpgradeableTokenV3.sol");
let web3 = require("web3");

const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

const name = "Test2";
const symbol = "TST2";
const supply = "2000000";

module.exports = async function (deployer) {

	  const existing = await NuNetUpgradeableTokenV2.deployed();
	  console.log("existing adress", existing.address);

      const instance = await upgradeProxy(existing.address, NuNetUpgradeableTokenV3,{deployer});
      
      console.log("Upgraded", instance.address);
};
