# nunet-upgradabletoken-contracts
Includes token contracts, migrations, tests

### NuNetUpgradeableToken
* ERC-20 upgradable implementation for NunetUpgradeableToken Token

## Deployed Contracts
* NunetUpgradeableToken (Mainnet): 
* NunetUpgradeableToken (Kovan): 
* NunetUpgradeableToken(Ropsten) : 

## Requirements

follows flow from 

https://www.npmjs.com/package/@openzeppelin/truffle-upgrades

## Install

### Dependencies
```bash
npm install
```

### Test 
```bash
npm run test
```

## Package
```bash
npm run package-npm
```

## Release

