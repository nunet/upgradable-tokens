"use strict";
let BigNumber = require("bignumber.js");
var  NuNetUpgradeableTokenV2 = artifacts.require("NuNetUpgradeableTokenV2.sol");
var  NuNetUpgradeableTokenV3 = artifacts.require("NuNetUpgradeableTokenV3.sol");
const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

let Contract = require("@truffle/contract");
const { assert } = require("chai");

var ethereumjsabi  = require('ethereumjs-abi');
var ethereumjsutil = require('ethereumjs-util');

async function testErrorRevert(prom)
{
    let rezE = -1
    try { await prom }
    catch(e) {
        rezE = e.message.indexOf('revert');
        //console.log("Catch Block: " + e.message);
    }
    assert(rezE >= 0, "Must generate error and error message must contain revert");
}
  
contract('NuNetUpgradeableTokenV3', function(accounts) {

    var nunetUpgradeableToken;
    const decimals = 6;
    const tokenFactor = (new BigNumber(10)).pow(decimals);
    let initialSupply = (new BigNumber(1000000)).times(tokenFactor).toFixed();
    
    before(async () => 
        {
            const name="NuNet Utility Token";
            const symbol="NTX";

            //nunetUpgradeableToken = await NuNetUpgradeableTokenV2.deployed();
            const nunetUpgradeableToken_Old = await deployProxy(NuNetUpgradeableTokenV2,[name, symbol, initialSupply] , { initializer: 'initialize'});

            // upgrade the Proxy

            nunetUpgradeableToken = await upgradeProxy(nunetUpgradeableToken_Old, NuNetUpgradeableTokenV3);

        });

        const getInitialSupplyAndVerify = async (_totalSupply) => {
            
            const totalSupply = await nunetUpgradeableToken.totalSupply.call()

            assert.equal((new BigNumber(totalSupply)).toFixed(), _totalSupply);
        }

        const getDecimalsAndVerify = async (_decimals) => {

            const decimals = await nunetUpgradeableToken.decimals.call()

            assert.equal(decimals.toNumber(), _decimals);

        }

        const transferAndVerify = async (_accountFrom, _accountTo, _amount) => {

            const _amountBN = new BigNumber(_amount);

            const sender_bal_b = (await nunetUpgradeableToken.balanceOf(_accountFrom));
            const receiver_bal_b = (await nunetUpgradeableToken.balanceOf(_accountTo));

            await nunetUpgradeableToken.transfer(_accountTo, _amountBN.toString(), {from:_accountFrom})

            const sender_bal_a = (await nunetUpgradeableToken.balanceOf(_accountFrom));
            const receiver_bal_a = (await nunetUpgradeableToken.balanceOf(_accountTo));

            assert.equal(_amountBN.plus(receiver_bal_b).isEqualTo(receiver_bal_a), true);
            assert.equal(_amountBN.plus(sender_bal_a).isEqualTo(sender_bal_b), true);

        }

        const getRandomNumber = (max) => {
            const min = 10; // To avoid zero rand number
            return Math.floor(Math.random() * (max - min) + min);
        }

        const sleep = async (sec) => {
            console.log("Waiting for cycle to complete...Secs - " + sec);
            return new Promise((resolve) => {
                setTimeout(resolve, sec * 1000);
              });
        }

    // ************************ Test Scenarios Starts From Here ********************************************

    it("0. Initial Deployment Configuration - Decimals, Additional Mint", async function() 
    {
        // accounts[0] -> Contract Owner

        // Try to call with additional mint with non owner - should fail
        await testErrorRevert(nunetUpgradeableToken.additionalMint(initialSupply, { from: accounts[1] }));

        // Call Upgraded contract to mint the additional tokens
        await nunetUpgradeableToken.additionalMint(initialSupply, { from: accounts[0] });

        // Try calling to mint again should fail
        await testErrorRevert(nunetUpgradeableToken.additionalMint(initialSupply, { from: accounts[0] }));

        // Check for the total Supply
        initialSupply = (new BigNumber(initialSupply)).times(2).toFixed();   // Tokens total supply should be doubled 
        await getInitialSupplyAndVerify(initialSupply);

        // Check for the Configured Decimals - Should be 6
        await getDecimalsAndVerify(decimals);

    });

    it("2. Transfer Token - Transfer to Different Account and Validation", async function() 
    {
        // accounts[0] -> Contract Owner

        // Transfer 100K tokens
        const transferAmountBN = new BigNumber("100000").times(tokenFactor);
        await transferAndVerify(accounts[0], accounts[1], transferAmountBN.toString());

    });

    it("3. Contract Owner Validation", async function() 
    {
        // accounts[0] -> Contract Owner

        const owner = await nunetUpgradeableToken.owner.call()
        assert.equal(owner, accounts[0]);

    });


});
